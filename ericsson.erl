-module(ericsson).
-import(rand, [seed/2, uniform/0]).
-import(lists, [seq/2, zip/2]).
-import(lists, [nth/2, sort/1]).
-export([encrypt/2, start/0]). %, decrypt/2]).

encrypt(D, S) ->
	seed(exsplus, {1, 2, S}),
	C = seq(32, 126),
	F = sort([{uniform(), X} || X <- C]),
	T = zip(C, [X || {_, X} <- F]),
	I = [spawn(fun R() -> 
			receive {Pid, Id, X} ->
				Pid ! {Id, 0},
				R()
			end
		end) || {X, 0} <- T],
	E = zip(seq(1, length(D)), D),
	P = self(),
	[nth(X - 31, I) ! {P, Q, X} ||
		{Q, X} <- E],
	G = [receive H -> H end || _ <- D],
	[X || {_, X} <- sort(G)].

start() ->
	D = "test",
	S = 4,
	io:fwrite("Test: %s\n", encrypt(S, D)).
