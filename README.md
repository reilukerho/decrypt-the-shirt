# Decrypt the Shirt

Ericsson provided us shirts while doing their hackathon challenge. 
These shirts contain Erlang encrypted code and on the back of the shirts is an 
encrypted string of characters. The challenge was to decrypt the string in 
question.